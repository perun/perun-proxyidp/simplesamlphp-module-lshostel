## [3.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-lshostel/compare/v3.0.0...v3.0.1) (2024-04-23)


### Bug Fixes

* mark as abandoned ([9a170f1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/simplesamlphp-module-lshostel/commit/9a170f1b548d7046520352dd582612de699c89a9))

# [3.0.0](https://gitlab.ics.muni.cz/perun-proxy-aai/simplesamlphp/simplesamlphp-module-lshostel/compare/v2.1.1...v3.0.0) (2023-05-07)


### Features

* 🎸 Remove register and pwd_reset links, remove pwd_reset ([65617bb](https://gitlab.ics.muni.cz/perun-proxy-aai/simplesamlphp/simplesamlphp-module-lshostel/commit/65617bb9d2f779427c3281475127e85d4bc87505))


### BREAKING CHANGES

* 🧨 removed displaying links, removed password reset, module DEPRECATED!

## [2.1.1](https://github.com/CESNET/lshostel-aai-proxy-idp-template/compare/v2.1.0...v2.1.1) (2022-05-20)


### Bug Fixes

* change version constraint for perun module ([1b99cc3](https://github.com/CESNET/lshostel-aai-proxy-idp-template/commit/1b99cc3e764514981ba7ffc4ed13a7c60586b00e))

# [2.1.0](https://github.com/CESNET/lshostel-aai-proxy-idp-template/compare/v2.0.0...v2.1.0) (2022-03-29)


### Features

* Semantic release ([#9](https://github.com/CESNET/lshostel-aai-proxy-idp-template/issues/9)) ([d391179](https://github.com/CESNET/lshostel-aai-proxy-idp-template/commit/d3911795ded519d57608c8002bd330c9afdfd65b))

# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

## [v1.3.0]
#### Changed
- Redesign LS Hostel theme

## [v1.2.0]
#### Added
- Added new filter which converts login domain to lowercase and trim whitespaces
- Added .gitignore

#### Changed
- Some fields on forms are now prefilled if the data are accessible 

## [v1.1.0]
#### Added
- Added possibility to recover LS Hostel password
#### Changed
- Use email as identifier instead of username


## [v1.0.0]
- First release
 
[Unreleased]: https://github.com/CESNET/lshostel-aai-proxy-idp-template/tree/master
[v1.3.0]: https://github.com/CESNET/lshostel-aai-proxy-idp-template/tree/v1.3.0
[v1.2.0]: https://github.com/CESNET/lshostel-aai-proxy-idp-template/tree/v1.2.0
[v1.1.0]: https://github.com/CESNET/lshostel-aai-proxy-idp-template/tree/v1.1.0
[v1.0.0]: https://github.com/CESNET/lshostel-aai-proxy-idp-template/tree/v1.0.0
