<?php

namespace SimpleSAML\Module\lshostel;

use SimpleSAML;
use SimpleSAML_XHTML_Template;

class TemplateHelper
{
    /**
     * Recursive attribute array listing function.
     *
     * @param SimpleSAML_XHTML_Template $t          Template object
     * @param array                     $attributes Attributes to be presented
     * @param string                    $nameParent Name of parent element
     *
     * @return string HTML representation of the attributes
     */
    public static function presentAttributes($t, $attributes, $nameParent)
    {
        $i = 0;
        $summary = 'summary="' . $t->t('{consent:consent:table_summary}') . '"';

        if (strlen($nameParent) > 0) {
            $parentStr = strtolower($nameParent) . '_';
            $str = '<table class="table attributes" ' . $summary . '>';
        } else {
            $parentStr = '';
            $str = '<table id="table_with_attributes" class="table attributes" ' . $summary . '>';
            $str .= "\n" . '<caption>' . $t->t('{consent:consent:table_caption}') .
                '</caption>';
        }

        foreach ($attributes as $name => $value) {
            $nameraw = $name;
            $name = $t->getAttributeTranslation($parentStr . $nameraw);

            if (preg_match('/^child_/', $nameraw)) {
                // insert child table
                $parentName = preg_replace('/^child_/', '', $nameraw);
                foreach ($value as $child) {
                    $str .= "\n" . '<tr class="odd"><td style="padding: 2em">' .
                        self::presentAttributes($t, $child, $parentName) . '</td></tr>';
                }
            } else {
                // insert values directly

                $str .= "\n" . '<tr><td><span class="attrname">' . htmlspecialchars($name) . '</span>';

                $isHidden = in_array($nameraw, $t->data['hiddenAttributes'], true);
                if ($isHidden) {
                    $hiddenId = SimpleSAML\Utils\Random::generateID();

                    $str .= '<div class="attrvalue" style="display: none;" id="hidden_' . $hiddenId . '">';
                } else {
                    $str .= '<div class="attrvalue">';
                }

                if (sizeof($value) > 1) {
                    // we hawe several values
                    $str .= '<ul>';
                    foreach ($value as $listitem) {
                        if ('jpegPhoto' === $nameraw) {
                            $str .= '<li><img src="data:image/jpeg;base64,' .
                                htmlspecialchars($listitem) .
                                '" alt="User photo" /></li>';
                        } else {
                            $str .= '<li>' . htmlspecialchars($listitem) . '</li>';
                        }
                    }
                    $str .= '</ul>';
                } elseif (isset($value[0])) {
                    // we hawe only one value
                    if ('jpegPhoto' === $nameraw) {
                        $str .= '<img src="data:image/jpeg;base64,' .
                            htmlspecialchars($value[0]) .
                            '" alt="User photo" />';
                    } else {
                        $str .= htmlspecialchars($value[0]);
                    }
                } // end of if multivalue
                $str .= '</div>';

                if ($isHidden) {
                    $str .= '<div class="attrvalue consent_showattribute" id="visible_' . $hiddenId . '">';
                    $str .= '... ';
                    $str .= '<a class="consent_showattributelink" href="javascript:SimpleSAML_show(\'hidden_' .
                        $hiddenId;
                    $str .= '\'); SimpleSAML_hide(\'visible_' . $hiddenId . '\');">';
                    $str .= $t->t('{consent:consent:show_attribute}');
                    $str .= '</a>';
                    $str .= '</div>';
                }

                $str .= '</td></tr>';
            }   // end else: not child table
        }   // end foreach
        $str .= isset($attributes) ? '</table>' : '';

        return $str;
    }

    /**
     * Write out one or more INPUT elements for the given name-value pair.
     *
     * If the value is a string, this function will write a single INPUT element.
     * If the value is an array, it will write multiple INPUT elements to recreate the array.
     *
     * @param string $name the name of the element
     * @param string|array $value the value of the element
     */
    public static function printItem($name, $value)
    {
        assert('is_string($name)');
        assert('is_string($value) || is_array($value)');

        if (is_string($value)) {
            echo '<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) .
                '" />';

            return;
        }

        // This is an array...
        foreach ($value as $index => $item) {
            self::printItem($name . '[' . $index . ']', $item);
        }
    }
}
