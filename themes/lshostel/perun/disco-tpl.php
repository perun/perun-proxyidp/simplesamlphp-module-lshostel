<?php

declare(strict_types=1);

use SimpleSAML\Module\lshostel\DiscoHelper;

/**
 * This is simple example of template for perun Discovery service.
 *
 * Allow type hinting in IDE
 *
 * @var sspmod_perun_DiscoTemplate $this
 */
$this->data['jquery'] = [
    'core' => true,
    'ui' => true,
    'css' => true,
];

$this->data['head'] = '<link rel="stylesheet" media="screen" type="text/css" href="' . SimpleSAML\Module::getModuleUrl(
    'discopower/style.css'
) . '" />';
$this->data['head'] .= '<link rel="stylesheet" media="screen" type="text/css" href="' . SimpleSAML\Module::getModuleUrl(
    'lshostel/res/css/disco.css'
) . '" />';

$this->data['head'] .= '<script type="text/javascript" src="' . SimpleSAML\Module::getModuleUrl(
    'discopower/js/jquery.livesearch.js'
) . '"></script>';
$this->data['head'] .= '<script type="text/javascript" src="' . SimpleSAML\Module::getModuleUrl(
    'discopower/js/suggest.js'
) . '"></script>';

$this->data['head'] .= DiscoHelper::searchScript();

$this->includeAtTemplateBase('includes/header.php');

$this->includeAtTemplateBase('includes/footer.php');
